package main;

import java.io.*;
import java.util.Scanner;
import serverSide.*;
import clientSide.*;

public class MainMenu
{
	public static void main(String args[]) throws IOException
	{
		Scanner reader = new Scanner(System.in);
		
		int playerOption;
		String playerName;
		
		System.out.println("Type your name:");
		playerName = reader.next();
		
		System.out.println("1. Host a Game");
		System.out.println("2. Play a Game");
		playerOption = reader.nextInt();
		
		//reader.close();
		
		if(playerOption == 1)
		{
			createGame(playerName);
		}else{
			searchGame(playerName);
		}
	}
	
	static public void createGame(String playerName)
	{
		HostGame newGame = new HostGame(playerName);
		
		newGame.createLobby();
	}
	
	static public void searchGame(String playerName) throws IOException
	{
		SearchGame newSearch = new SearchGame(playerName);
		
		newSearch.createSearch();
	}
}
