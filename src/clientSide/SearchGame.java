package clientSide;

import java.io.*;
import java.net.*;

public class SearchGame
{
	public String playerName;
	private int port = 48450;
	
	public SearchGame(String playerName)
	{
		this.playerName = playerName;
	}
	
	public void createSearch() throws IOException
	{
		byte[] receiveBuffer = new byte[15000];
		
		String hostIPAddress;
		String data;
		
		DatagramSocket clientSocket = new DatagramSocket(port, InetAddress.getByName("0.0.0.0"));
		clientSocket.setBroadcast(true);
		
		DatagramPacket packet = new DatagramPacket(receiveBuffer, receiveBuffer.length);
		
		System.out.println("Looking for a game...");
		
		clientSocket.receive(packet);

		hostIPAddress = packet.getAddress().getHostAddress();
		System.out.println("Discovered package from " + packet.getAddress().getHostAddress());
		
		//TCP
		String msg;
		
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(
				System.in));
		
		Socket clientSocketTCP = new Socket(hostIPAddress, port);
		
		DataOutputStream outToServer = new DataOutputStream(clientSocketTCP.getOutputStream());
		
		msg = playerName;
		
		System.out.println("Sending MSG");
		outToServer.writeBytes(msg);
		
		clientSocket.close();
		
		
		
		
		//data = new String(packet.getData());
		//System.out.println(data + " will host this match.");
		
		//ClientMatch newMatch = new ClientMatch(hostIPAddress, port, playerName);
		//newMatch.startClientMatch();
	}
}
