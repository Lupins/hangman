package clientSide;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.io.*;

public class ClientMatch
{
	String hostIPAddress;
	int port;
	String playerName;
	
	public ClientMatch(String hostIPAddress, int port, String playerName)
	{
		this.hostIPAddress = hostIPAddress;
		this.port = port;
		this.playerName = playerName;
	}
	
	public void startClientMatch() throws IOException
	{
		String output;
		Socket clientSocket = new Socket(hostIPAddress, port);
		
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		outToServer.writeBytes(playerName);
		
		while(true)
		{
			//If the Match is over
			if(inFromServer.readLine().compareTo("MatchOver") == 0)
				break;
			
			//Player Turn to choose between type a character or guess the entire word
			if(inFromServer.readLine().compareTo("Turn") == 0)
			{
				
			}else{
				System.out.println(inFromServer.readLine());
			}
		}
	}
}
