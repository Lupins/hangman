package serverSide;

import java.io.IOException;
import java.util.Scanner;

public class HostGame {

    public String playerName;

    public HostGame(String playerName) {
        this.playerName = playerName;
    }

    public void createLobby() {
        Scanner input = new Scanner(System.in);
        String secretWord;
        String firstTip;
        int maxNumPlayers;

        System.out.println("Maximum number of Players: ");
        maxNumPlayers = input.nextInt();

        System.out.println("Secret Word: ");
        secretWord = input.next();

        System.out.println("Type the first Tip: ");
        firstTip = input.next();

        Thread t = new Thread(new ServerBroadcast());
        t.start();

        Lobby newLobby = new Lobby(secretWord, firstTip, maxNumPlayers, playerName, "Broadcast-2");

        try {
            newLobby.createLobby();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
