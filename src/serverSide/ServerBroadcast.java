package serverSide;

import java.io.IOException;
import java.net.*;

public class ServerBroadcast implements Runnable {

    public static DatagramSocket serverSocket;

    private int port = 48450;

    public void run() {
        byte[] sendData = "Host".getBytes();

        DatagramPacket sendPacket;
        serverSocket = null;

        //System.out.println("Running " + threadName);
        try {
            while (true) {
                for (int i = 0; i < 5; i++) {
                    //System.out.println("Broadcasting the match... " + i);

                    //System.out.println("Waiting for players...");

                    serverSocket = new DatagramSocket();
                    sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), port);

                    serverSocket.send(sendPacket);

                }
                Thread.sleep(5000);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            //System.out.println("Thread " + threadName + " interrupted.");
        }

        //System.out.println("Thread " + threadName + " exiting.");
    }

    public static void closeSocket() {
        serverSocket.close();
    }
}
