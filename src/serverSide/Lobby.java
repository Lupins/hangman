package serverSide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class Lobby// implements Runnable
{
	public Thread t;
	public String threadName;
	
	private int port = 48450;
	
	private String secretWord;
	private String firstTip;
	private int maxNumPlayers;
	private String playerName;
	
	private String[] playerAdress;
	
	
	//Par de informações do jogador: NomeDoJogador, IPdoJogador
	//private Pair<String, String>[] players;
	
	public Lobby(String secretWord, String firstTip, int maxNumPlayers, String playerName, String threadName)
	{
		this.secretWord = secretWord;
		this.firstTip = firstTip;
		this.maxNumPlayers = maxNumPlayers;
		this.playerName = playerName;
		
		this.threadName = threadName;
	}
	
	public void createLobby() throws IOException
	{
		int playersInLobby = 0;
		
		String clientName;

		byte[] receiveBuffer = new byte[15000];

		DatagramSocket serverSocket = new DatagramSocket();
		serverSocket.setBroadcast(true);

		System.out.println("Waiting for " + playersInLobby + "|" + maxNumPlayers + " players.");
		
		while(playersInLobby < maxNumPlayers)
		{	
			//TCP
			ServerSocket welcomeSocket = new ServerSocket(port);
			
			Socket serverSocketTCP = welcomeSocket.accept();
			System.out.println("Recebeu");
			
			playersInLobby++;
			
			System.out.println("Waiting for " + playersInLobby + "|" + maxNumPlayers + " players.");
			
			BufferedReader inFromClient = new BufferedReader(
					new InputStreamReader(serverSocketTCP.getInputStream()));
			
			System.out.println("Address1: " + welcomeSocket.getInetAddress());
			System.out.println("Address2: " + welcomeSocket.getLocalSocketAddress());
			System.out.println("Address3: " + serverSocketTCP.getInetAddress());
			
			System.out.println("Esperando MSG");
			String clientSentence = inFromClient.readLine();
			
			System.out.println(":" + clientSentence);
			
			serverSocketTCP.close();
			
			
			
			
			
			
			//System.out.println("Response from client: " + receivePacket.getAddress().getHostAddress());
			//playerAdress[playersInLobby++] = receivePacket.getAddress().getHostAddress();
			
			//clientName = new String(receivePacket.getData(), "UTF-8");
			//System.out.println(clientName + " joinned the match.");
		}

		//ServerMatch newMatch = new ServerMatch(playerAdress);
		//newMatch.startMatch();
		//Cria um par com o nome do jogador e IP respeitando o número máximo de jogadores
		//Pair[ playerInLobby <= maxNumPlayers ] = new Pair(playerName, receivePacket.getAddress().getHostAddress());
		
		//Criar uma partida
		//ServerMatch newMatch = new ServerMatch(Pair[] players, String secretWord, String firstTip, String playerName);
		//newMatch.startServerMatch();
	}
	
	/*public void run()
	{
		System.out.println("Running " + threadName);
		
		try
		{
			createLobby();
		}catch (IOException e){
			System.out.println("Thread " + threadName + " interrupted.");
		}
		
		System.out.println("Thread " + threadName + " exiting.");
	}
	
	public void start()
	{
		System.out.println("Starting " + threadName);
		
		if(t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}*/
}
